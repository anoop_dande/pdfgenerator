package com.epam.PEPCertificate;

import java.io.ByteArrayOutputStream;
import java.io.StringReader;
import java.util.Iterator;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;

@Component
public class PdfGenaratorUtil {
	@Autowired
	private TemplateEngine templateEngine;
	public HttpEntity<byte[]> createPdf(String templateName, Map map) throws Exception {
		Context context = new Context();
		if (map != null) {
		     Iterator itMap = map.entrySet().iterator();
		       while (itMap.hasNext()) {
		    	  @SuppressWarnings("rawtypes")
				Map.Entry pair = (Map.Entry) itMap.next();
		          context.setVariable(pair.getKey().toString(), pair.getValue());
			}
		}
		
		String processedHtml = templateEngine.process(templateName, context);
		String fileName = "PEPCertificate";
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		baos = generatePdf(processedHtml);

		HttpHeaders header = new HttpHeaders();
	    header.setContentType(MediaType.APPLICATION_PDF);
	    header.set(HttpHeaders.CONTENT_DISPOSITION,
	                   "attachment; filename=" + fileName+".pdf".replace(" ", "_"));
	    header.setContentLength(baos.toByteArray().length);

	    return new HttpEntity<byte[]>(baos.toByteArray(), header);

	}

	public ByteArrayOutputStream generatePdf(String html) {

		String pdfFilePath = "";
		PdfWriter pdfWriter = null;

		Document document = new Document();
		try {

			document = new Document();
			document.addAuthor("Anoop Dande");
			document.addTitle("Reference Letters");
			document.setPageSize(PageSize.A4_LANDSCAPE);
			//document.setMargins(40,40,100,100);

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			
			pdfWriter=PdfWriter.getInstance(document, baos);
			HeaderFooterPageEvent event = new HeaderFooterPageEvent();
			pdfWriter.setPageEvent(event);
			

			document.open();
			XMLWorkerHelper xmlWorkerHelper = XMLWorkerHelper.getInstance();
			xmlWorkerHelper.getDefaultCssResolver(true);
			xmlWorkerHelper.parseXHtml(pdfWriter, document, new StringReader(
					html));
			document.close();
			System.out.println("PDF generated successfully");

			return baos;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

	}
}