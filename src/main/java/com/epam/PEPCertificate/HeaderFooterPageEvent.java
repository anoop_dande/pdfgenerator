package com.epam.PEPCertificate;

import java.io.IOException;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.ElementList;
import com.itextpdf.tool.xml.XMLWorkerHelper;

public class HeaderFooterPageEvent extends PdfPageEventHelper {

	public void onStartPage(PdfWriter writer, Document document) {

		PdfContentByte cb = writer.getDirectContent();
		Image imgSoc = null;
		try {
			imgSoc = Image.getInstance(getClass().getClassLoader().getResource("pepheader.png"));
		} catch (BadElementException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//imgSoc.scaleToFit(80, 200);
		imgSoc.setAbsolutePosition(0, 563);

		try {
			cb.addImage(imgSoc);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	public void onEndPage(PdfWriter writer, Document document) {

    	PdfContentByte cb = writer.getDirectContent();    
        Image imgSoc = null;
		try {
			imgSoc = Image.getInstance(getClass().getClassLoader().getResource("pepfooter.png"));
		} catch (BadElementException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //imgSoc.scaleToFit(550,550);
        imgSoc.setAbsolutePosition(0, 0);

        try {
			cb.addImage(imgSoc);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}