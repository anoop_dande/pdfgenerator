package com.epam.PEPCertificate;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReferenceLettersController {

	@Autowired
	PdfGenaratorUtil pdfGenaratorUtil;

    @GetMapping(value="requestPDFDownload")
    public HttpEntity<byte[]> generatePDF() throws Exception {
    	Map<String,String> data = new HashMap<String,String>();
        data.put("name","Anoop Kumar Dande");
        data.put("startDate","26th Feb 2019");
        data.put("endDate","26th June 2019");
        data.put("presentDate","June 28, 2019");
        data.put("topics","OOPS Design");
		System.out.println("in controller");
        return pdfGenaratorUtil.createPdf("address",data);
    }

}
