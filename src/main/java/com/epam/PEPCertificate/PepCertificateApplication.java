package com.epam.PEPCertificate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PepCertificateApplication {

	public static void main(String[] args) {
		SpringApplication.run(PepCertificateApplication.class, args);
	}

}
